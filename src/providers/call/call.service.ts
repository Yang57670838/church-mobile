import { Injectable } from '@angular/core';
import { CallNumber } from '@ionic-native/call-number';
import { Platform } from 'ionic-angular';

@Injectable()
export class CallService {

  constructor(private callNum: CallNumber, private platform: Platform) {
    console.log('Hello CallService Provider');
  }

  async makeCall(phone: string) : Promise<any> {

     await this.platform.ready();
     try {
        await this.callNum.callNumber(phone, true);
        console.log('Opened dialer.');
        return Promise.resolve();
     }
     catch (e) {
       console.error(e || 'Error launching dialer.');
       return Promise.reject('Error launching dialer.')
     }
     
  }
}
