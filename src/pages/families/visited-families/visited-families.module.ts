import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VisitedFamiliesPage } from './visited-families';

@NgModule({
  declarations: [
    VisitedFamiliesPage,
  ],
  imports: [
    IonicPageModule.forChild(VisitedFamiliesPage),
  ],
})
export class VisitedFamiliesPageModule {}
