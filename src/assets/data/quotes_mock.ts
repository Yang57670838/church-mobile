export default [
  {
    id: '324211',
    person: 'Theodore Roosevelt',
    text: 'Believe you can and you\'re halfway there',
    time: '2000-00-00 00:00:00'
  },
  {
    id: '32422',
    person: 'Norman Vincent Peale',
    text: 'Change your thoughts and you change your world.',
    time: '2000-00-00 00:00:00'
  },
  {
    id: '324323',
    person: 'Robert H. Schuller',
    text: 'What great thing would you attempt if you knew you could not fail?',
    time: '2000-00-00 00:00:00'
  },
  {
    id: '432432',
    person: 'John Wooden',
    text: 'Ability may get you to the top, but it takes character to keep you there.',
    time: '2000-00-00 00:00:00'
  },
  {
    id: '532432',
    person: 'Robert Frost',
    text: 'Education is the ability to listen to almost anything without losing your temper.',
    time: '2000-00-00 00:00:00'
  }
];
