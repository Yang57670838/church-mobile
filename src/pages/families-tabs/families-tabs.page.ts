import {Component} from '@angular/core';
import {VisitedFamiliesPage} from '../families/visited-families/visited-families'
import {SurburbsPage} from '../surburbs/surburbs'

@Component({
    selector: 'page-families-tabs',
    template: `
    <ion-tabs>    
            <ion-tab [root] = 'visitedFamiliesPage' tabTitle="已访家庭" tabIcon='walk'></ion-tab>
            <ion-tab [root] = 'surburbsPage' tabTitle="所有教友" tabIcon='book'></ion-tab>
    </ion-tabs>`
})
export class FamiliesTabsPage {
    visitedFamiliesPage = VisitedFamiliesPage;
    surburbsPage = SurburbsPage;
}