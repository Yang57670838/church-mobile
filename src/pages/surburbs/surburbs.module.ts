import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SurburbsPage } from './surburbs';

@NgModule({
  declarations: [
    SurburbsPage,
  ],
  imports: [
    IonicPageModule.forChild(SurburbsPage),
  ],
})
export class SurburbsPageModule {}
