import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Device } from '@ionic-native/device';
// import $fh from 'fh-js-sdk';

@Injectable()
export class DeviceService {
  constructor(private device: Device, private platform: Platform) {}

  /**
   * Device ID from the device itself or provided by FeedHenry when device is not available
   */
  getDeviceId() {
    if (!this.isDevice()) {
        console.log('no real device ID, ')
        // return $fh.getFHParams().cuid;
    }

    return this.device.uuid;
  }

  isDevice() {
    return this.platform.is('cordova') || this.platform.is('mobile');
  }

  isIos() {
    return (this.isDevice() && this.platform.is('ios'));
  }
}
