export interface Photo {
    rawData: string;
    description: string;
    randomID: number;
    lat: number;
    lon: number;
    maker: string;
    thumbnailData?: string;
}