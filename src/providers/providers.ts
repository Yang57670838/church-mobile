import { DeviceService } from './device/device.service';
import { PhotoService } from './photo/photo.service';
import { CallService } from './call/call.service';

export {
    DeviceService,
    PhotoService,
    CallService
};
