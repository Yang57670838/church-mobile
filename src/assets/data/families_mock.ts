export default [
    {
      familyID: '213213',
      users: [
        {
          userId: '1',
          name: 'Theodore Roosevelt',
          phone: '1234567'
        },
        {
          userId: '2',
          name: 'Norman Vincent Peale',
          phone: '1234567'
        },
        {
          userId: '3',
          name: 'Robert H. Schuller',
          phone: '1234567'
        }
      ],
      surburb: 3083,
      address: 'robot street'
    },
    {
      familyID: '21321321',
      users: [
        {
          userId: '4',
          name: 'John Wooden',
          phone: '1234567'
        },
        {
          userId: '5',
          name: 'Robert Frost',
          phone: '1234567'
        }
      ],
      surburb: 3000,
      address: 'robot street'
    },
    {
      familyID: '12321321',
      users: [
        {
          userId: '6',
          name: 'Benjamin Disraeli',
          phone: '1234567'
        },
        {
          userId: '7',
          name: 'Norman Vincent Peale',
          phone: '1234567'
        }
      ],
      surburb: 3105,
      address: 'robot street'
    }
  ];
  