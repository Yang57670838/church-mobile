import { Injectable } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { DeviceService } from '../device/device.service';
import {Photo} from '../../assets/data/photo.interface';

@Injectable()
export class PhotoService {

  thumbnailTargetHeight: number = 50;
  thumbnailTargetWidth: number = 50;
  FAKE_PHOTO_JPEG : string = '/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wgARCACyALIDAREAAhEBAxEB/8QAGwABAAIDAQEAAAAAAAAAAAAAAAUGAgMEBwH/xAAaAQEAAgMBAAAAAAAAAAAAAAAAAgMBBAUG/9oADAMBAAIQAxAAAAEAAAD4c5zmJtOo2AAAAAAAHIQg1ZSujLuollhz2Yi9uPDvQk5pc+gAAAAxK6dXMsuvEtk9aQAA1SxVupXU+5VPW47gAAAaisVrz5zYsPPmAAAAOeePO/R0b+jCXAABiVWt6L5bZldWQAAAAA+HnPpKN3TrkwCPK4aTKrPfr5zwyxn6ZYfcAB8PmWOXzLHONM0btR6CQLKVIjQAAACZ0pS+pOpdarCQAAACbOE4gAAAb63sXjtrbHNG7lNK7dQAAAH07ThAAABLakvWPJ7IrPSr839LQAAABuOwjQAAATWjL1Ty2yK/0IeY+n1wAAALCYECAAAC5ca2+cG4cluPHfY6vzIAADIvhHFXg1TAACZ0pel+Z2OqvIFP7FVG7lOqYADZF2ZXDLhNvDts3Msj9iOiwOivMnqy7acgADDKJ24cd2NcmeHZTmR1peXez1bYayG5s/S/NbAAAAAAAAAh9yFI9Xr2EFUg9V8btbI5ApXapuvFuAAAFf6EOunMrqyAo3cp19urvBDkz56628m3VLFP7Fdf6FfqvldkAAAVzo10nt1ejecvk9aWuWPKfYa1stwB8KpVn1LyOzE7ceS3Fb6VeWF64V0jryAGqWKp1q6n1q7xw7Y/YxcuNbSe3TyduqVABwkfzp+leZ2Mj4VTrVUvtVZxSGvLPDktxHbOLPy53fiXdFeRA70KP6jXtGQAAiTDmWeiecvzwHwi9qMbsxxy7acy+nLbHIEFvQofpte0TZgAAEYRmpK/eevltSQAAA1SxS+1VBdqqy5ZgAAAGkr2HToTsvLsmtKfZTn6apYi9qNe6MIDq1y1uJUAAAAAA1EWcJqwYfZOskjuAAAAAAAAAAAAAAAP/8QAKRAAAgICAAYCAQQDAAAAAAAAAgQBAwAFEBESExUwFCAGITQ1QBYkUP/aAAgBAQABBQL0kxSOfOXz59GQ3ROCYl/QtYrpyzYnOAm83gaC6cj8frzwFOF+P5ZpXKs77SxVbCs8ieceqZgYvfksV097GLoLK/c6wsFrR1nk/J19lDIXx6LLBrAjufuQ1Vaseq6mu+t7WWJEo33fuUwImVjzCKIJVeyYiY2eulQlGe8PFpmFxJu8sK6wxpYtXLyrueXezzD2eZdzzTueaczzTmeaczzTmeadzzLueYdzy72eWew9i3YIlIF8m7KHzgseLm16U9bc5lugsECEgL0g4Qg5+69FNfeurAaq83y8R63P3fo1n8jw3n8f6en9H45NejUj1bPhuh56301rc6tkH6+jQ0c7ODNXfWmJGfuIyRRHSL/7WKzkPsrrGGSooBanjs9TNxnUdU/UKzsJOP8AcxyOav4+WWIq24WlTnPAq54FXB0yUZUovR9yATg9UkeTo1JzwKuRo1IwNSkGdNa9SEc2sMeoNLZ29h/S2lva12tHjdzVfA4sr4ls7S3Ho27Nqymva+Wpx39/6p19tbhsKuqrRtddGHYFYv7kejVlUD/o3J0wii8aVq76zXAzGuvmTz/GY5xzPXu03BfS/rIeOjRr1zsNWako7e1WF3l2frZYFQtbwBwK2NiwGlWhf/Hx6hjpHdu88Ro7VX0Zo79etflK6JiY4OaSu3L1L1prfaqyN05Gecbw9q6eRFzNiujsPKaK16+Gy2EJ1KUTfZ9m1O7Gu2ZKSJCY8bdapbk6JScjQq5XqEgwKwrjjsNlWmNddjtwjAD92VBuxZxjXWKbGhuPQdgVg7u+eUKHeQjAD6bKgtG1AwyjbNrZTvFjytte3iVgBlu0Tqy/fzOT8p46EQq9x1hZB66ucLXWRnw2Rz4zc5GvvnB1uVqU1/8AA//EACURAAIBAwMFAQADAAAAAAAAAAABAgMRExIgMBAhMUBRQSIyUP/aAAgBAwEBPwH31FsxMxGJGIdNlueNNsUUuB0/g1bjSuRhbjauShbhSuRjbmnC3AnY1yNcjXIySMjMjMjMjMjMjMkjXI1yNcjU+eMGx0/VXWov31IeetTx6kP7danj1Ka/erV16ag2JW2ThfuuOkaUY0Y0Y0Y0JJcGhGNGNGNGiJ42U339Ob7bFt1/y4ZuyIu62VH+bab/ADrKp8IeeGpaxGVhST6t3e1OwnclDUKmiULEZ2FJPc6nw7yZjVjF0qS/N8JW2Sp/BxaNTMjMjNbO7FT+iVus5W4YTttcEY0Y0aFtlOx54oyaFJPilU+cym0Koi62a0Op8G2/9b//xAA0EQABAwICBwcCBgMAAAAAAAACAAEDBBEFIRIVIDAxMlEQEyJAQVKxQoEUMzRDYZFQcfD/2gAIAQIBAT8B8/JWwR8SR4vG3KKfGD9BWuJPahxj3AgxSAuOSCQTa4vff1GJxx5Bm6mrJpuZ9sTIHuLqDFTHKXNRTBK2kD7uSQYx0i4KrxA5vCOQ7uOU4i0gdUdeM/hLItzJIMY6RcFVVRVBXfhvWe2bKgru+bQPm+dxLCErWNrrV9N7Vq6m9vytW03t+Vq2m6LVlP0Wq6fotV0/Rarp+i1XT9Fqyn6LVlN0Wrab2rVtN7flavpvb8oaKAXuw7+pro6fJ83UeLg7+MbISYmu3lJD0Ac+iM3MnIuzCJnzifyld+nLtwr8/wC3lMRe1MXbhj2qG8pi8thaPtgk7qRj6Jnvm27cxYtF3z26iuihbjd1NKUpuZbFDiDRt3cvBAYm1xe+0RiDXJ9jGB5CQVc4cpJsUqG9Vrefoy1tP0ZPidQ/qpKiWTmLbEnF7ihxCoH6k2Kzt0Wt5+jf990+Kz/wixCoL6lcpSZie+xigaUF+nk6CPTqB2JAYxcX9UYuBOL7A0INSOZ83Hc4dCEstjVZT9xK4tw2MIiyeV/9bOKwaJ963r2CBG9hZUmGPfTm/pV4mUDsG5wwZO+0h4eqq6UagbPxU1HNDzN2ALmTCyhiaKNgb02ZomlBwdSxlEbgSo678Mzto3UuKylkDWVHXjP4SyJVWHBN4hydTUksPM2yAEb2FrqnwonzlyRHDSx9GRYpN3mkPDotcPbkTvd7rC6X94vtt19H346Q8zJ2tk/bTYoYeGXNlFURTcjo6SA+YU+F07+i1VT/AMocPpx+lXjiHoyqMVEcos1JKcpaRv20NG85XflZM1ms24rqBpvGHN8ohcXs+wFdUBwJNi07dFreboyPEagvVEZG9ye+xR0JTvd8hQAIDojw3VTRx1DZ8VUUUsHHhuRAjewsqXC/qm/pMzNk29mw2GTNsnUmFTDy5o6eUOYX7RAi5WQYfUH9Kiwhv3HUUMcTWBreSdmfitAen+D/AP/EADoQAAECAgQLBwIGAwEAAAAAAAEAAgMREiExURAgIjAyM0FSYXGRBBMjQnKBkqGiQENigrHRFFCywf/aAAgBAQAGPwLM1xG9Vp/RaR6LWBZLgeX4DKdXcvDFFToPle6peJGY3lWq47vYLXPWR2jq1TbRf6SqMSlyeFJ+QVMZuZMgqMGoXqnGPdt42leHDFLeNZx6L2hwuIRd2c0HbpsVF4I4Gwqqp12ZLnGpCHDaeDUHvk+Lfdm6EVoc1d7DJdCv2tVB+n/OOSbAhDhj0hSFbzpOzsjWF30HVH7VRdpj64l7jYtYRyUnPcQi6E6iSteegWv+0LXfaFrB8QtNvxWk34rSb8VpN+K0m/FabfitYPiFrR8Qtd9oWv8AtCLXRiQbRJTaZFa13VSi1tvwO4VZqkMmHvFThxg83ESRa4ScLRmmi4KJmWQx5jJNYwSa0SGCH2gWnJdm35mB6sP7xmzxzMLhM/TC7gQc003hMie2ZiRzYBRGGJC3giDaMwGi0oC5Hmi8NJaLTjjJLGbzk2FDsGIY/Z9M6Tb1KIxzTxGNRY0uNwTPfA9R2cisuAzpJVNc3k5acXqP6WlF6haDnc3Lw4LGm+WPJzQ4XFagDkZL8wfuWnG6j+lbEPutTPmU4sY1oAnUJKdwwObeJKifOC38HF4iinv9sPeN2GmE17bHCYxGwobvBpUJX5kOhGRLpTTXnTFTueJDgDZlFNvNeGmLWo9nccplY5YKT3BovKMLstZNr0x0YyGznmSyIcs6AVIVsOk1ZETK3Tbgc9xk1omUXu8xmeWJIoPbss4hNisOS5Nd3pYQJWTU4rjF+gReyboN9yDIniQ/qF4cQT3Tbi0ojw0cSi3swpHeNi2vebSdi7t8y/f2qrtBl6UBcv8AFYfX/SpHSdiy8wsRhxdUTX+kqYrGEv7Oe7du7F4sMjjsWRHf71rSaebV+X8VriPSJLzxH9UHdoNBu6LVQhNDW4aLa4zrBdxXexKxPbtOPTZp/wAruY0zC/5Qc0gtO0YmVAbP9NStijkVpxeoWqpeoqTGho4DEojKjbt3NGJEJ4uQa0SAzFIVP/lUfLtYVkuk/cOZpPcGtG0os7L8z/4u8izkb7SqLRIZqTxNUoRpD6qi/LA2PtXiB0M9VkRmH3w5b2t5la4O9NalAhS4vVJ5c7ibFN2U7PZbQVkktWS5pVQ6OVjvkq6I91lxOgWjM8f9B//EACoQAQACAAIJBAMBAQAAAAAAAAEAESExMEFRYXGRocHRECCB8LHh8UBQ/9oACAEBAAE/IdBlnPI5ENZ4Of1My/5hIHfE1/4D+U4swsRtcWU1raadZ0wL4QpjXckdXySeN/fMAaOP1qYcCaoF2mZABBHWaN2YM1jTdzLwlYx8fC8wwsD3oyX9qIP8jD7kHO1OBK9vL0NCYRyC3C/LOhi3D50apo1MqiDgep5gkoNV95U6C1j+W2u6zAv8p/WlRAIUjrlZmzIzfiY+bfc2+wRRqTvFbdwwTcyhYyM1KBlAftdIB9f4m4ffdPrXafwE/np/NT+an81P4D0m+5do/b9oprfTdBLnSDE5QYwNZMWIU12xiTOMbUQctEfgk1teG2JBXffVjt20mrRWlyCdV20K5uO/FmCEA9Gg0nG2d9FjOpPxoRf2Zepsbu5ohIlj2B6aH7EYvWy/3K76JNlr0mDMqv8AeehYV1Q4v3f6ntxDjqhlolI6tBnOFEMHIUQfGIvWqEwPfv8ALKuRrhEUfy7/AGBgcB4jfN1YqvcTXdTbGAcx/B9KfsL5Mw13XX9THULrLOZOrsd4rqZAc34+KZrxh2iF7IMXP3t3XU2Rm1G+/CZc8D9PSpn3FHxELCt8yxByCGEY9ofvP03mUU+FzTPt/juN41PnD8XMfhD1rNQwbKycB9mEqFU4F+7NDUL91yKXtLlHsNa99Yd5QU1n59actrwn5qFfh/PozH9bRHqOApquEr812cti9DjjUQzs7RmOV2n7hgELd/t6XpgTdKqYcEGrt7AYLEpIa9u/wJciCyZQ8mByuWgWx+nWFWr5cfmAEjkXyWDmK7jk9u9g9UP3D9D41xrXAEN+whUM0lq27dHEj2c/O5SKtKtzm7bFnSMA9bwPadbDGcuywR6kBIIWJr9EERLGIALrZ/Erw/zXzCwAGQqcmHY8GdorlThBtJdxFABXFxzeY2vECFGo18fW7IDzEc2DZfesKMOZBnFFb/1ht0sSx9UEpLGJqDa8MYw4V7kE4v8AbZEbxNqs3D/qPYorYw/JD/wttr3EogCg0BuDt9XFE9VtsLvIGNt4L8bdCyzwJRKw01JfhLwBrWEIAMg0VSo1bSP4AxrIRS/4w+ZRm8RTmeJyJw3yl3l6E2BuiDNo2fYl1zSeRMGJwRw/UovmsjTUjipMT68Tqy4Qzd8BMs/IfMR6iPGSxY3zH/wP/9oADAMBAAIAAwAAABAAAACASQAAAAAAASBrsmAAAAAACIAAC+QAAACdAAAAAfwAAAdAAAAAAIAASTdqB23rAySQAAAAChgAAAAQAAAB0cAAAASAAAAIDgAAACQAAADgAAAAACAAAAADQAAAQBgAAfAC8AAVAfsCQAABdqnQTgAAAAAAAAGAEgDgAAARABwAMVMAAABpAwACUZtgAOgwGAAT4Ql8+MgYAAAAICLIUAJwAAAQAAAAC0QAAAADDSCsCAAAAAAASCgAAAAAAAAAAAAAAAAf/8QAIBEBAAICAgMBAQEAAAAAAAAAAQARITEgMBBBYUBRUP/aAAgBAwEBPxD9+mIexlP7PpH+GBijffkHE1hzQcMNzCqnrRUQs3fWApjZmulFRAPdsmuhMifSfafafTmqr9J9p9p9ouUvfkfUUMMRGn8gtCABR41fkafz5seR+TmfK4Oymr6IAUcLUETfIFwcFsi+yPl8jpDmg7ivrx/fxhepQMcKK/jofBU3Bss4Kamul8Ez3DVxXlvChlhVUICvSsLEdzWPhQLZcPFlZAFkuXcFvMbM1Fwck1DxUMsMxAQGEyl7hiejzt064A5wmwIDph4VX3Mn9juYAUeadG+nQdQRLOCfUfHh+oAa4Fj7iqt6tLNb0qBbPR3fvRe8QbT5UNxH3P4plF/Fct/w/wD/xAAqEQEAAAIIBgMBAQEAAAAAAAABABEhMVFhcZHB0SAwQYGh4RBAsfDxUP/aAAgBAgEBPxD79ElOwpfGsVpOMjeENCd31B1B5j0j61ii04jacSMhcz5850479e2cMNXYUHvvPjnDDdREsKS0odnxFP0fmJy1zyEMuAdXHb95ZCQYCMA6OG3JZvIRV0FRZ75qISSQUyoeG9ufICSwhTpzd4X990fyd0P+13ht2bF6zYvWcXrOL1nF+zYLTm7wf7HeP5O6AvfdBcwlVe/Pa7Sa2RJaC0Z6EBXmNT9Q2uhcoSKas34QMoKTXT6ilh/Lk8Wn1MWSPJ8mK0TxPT6gh1rNwKs9Pm/Z46+IAFQ8sYAVR1eNZ2BrZCo0v9LgM1SpsubrIkxC5nxShAtaIESZ80wXn5LWKJXOZkziug4hpKD0DvC2w7xVwMA1nFDMlk6MquObBG6iKLJ8Qf0nFbTdtmP8BguoHZ1Yo6hgBpOJ6CUKWdfBNz1Dpr9OVOgzyp/ZcFQ0EzitBGT24B4qM1nUN8eS+GYE5dw1jrIUmHqrgl9Uofrpwy8aKDibn4/E4ZbCmCmoKt22cFfNa7ZdZf1XJcRoVuknWKlwqbPUMNTaUnrvL4GGasiOks/17vDV9vixgQZJCyUFnXJzk0QfIeb5o8QUaI4bQ06e4mv7DXTtKTPfhm7K6mFx5LCvOo8xTKQVBW4Wv8w2pdw73wUdE8aMpaxOl1hiju1Oh34xwAvLNoRIST4FGZAgF919/t8GTFu65VxTIzycyUVUju6zgD29RTZNir+soZky7QQMnNa1bviHEw/NQ2svuNbIAAkHIEaHw9r84TnJOnyKMyKJdL6f2cEUi7OiQrVkO8UPRwD/AGJwCvZ8Hc0W3G/SDRyHKohl0JX7IYmZ2irvZ35JFivQiSmW1dDOAAJBzEEkxOwwqsqspRT7Dk+aPMVMOz+wkvhOSOBOKokL6PfiDUz7jd2Ikrj+ra36VSJwBSDL/h//xAApEAEAAQMCBQUBAQEBAQAAAAABEQAhMUFRYXGBkaEQIDCxwdHwQPHh/9oACAEBAAE/EPgUCoAytKoFMgF2KZg/0OlCty5/wpcAP+mQqBM3H6f8FiVsl16adYpFx+f+eeadke5Ze5ZJyKJGTyKnegDfyD5WlFm8V/KaFuafoPxTSfuGEdPomoVeamU4LD2aQRNpMvXJ170GFpEkTn8Z2jlUAVOgmFnaaffKnUrwz1mOfY046IcrvLjpB78jcgdppoeYVRwf6HComVZxPdFnmdanBES+/MdT4QzHzquxu0Vx2GG5jq2KBLJmS2xdeK/L49D6rcdxyPEq9lDKNCP0typcxGwcuzw94x3EYCh8ccwGu1a66FHgFFi72NhofK58QUgcialWytVG5sTxYdMbSTiA/gfvsAznytg3cPumoLSEnamj2ivRipKIKFlMXGsN1Upgn1SoWU51AMvzoAZbn/OgdTn7b776ZXA8v4UvguVJeAcquU6Y1S12IUyNMUfnhKDlc5qI0iGI4tsnmhAIiNxKRD2Eg/a/E5acCYWoTR0ONGnSeyBgnnHOnfQJhRo/FfKyvACtVt+nwtRBbZAT0maGoAHQD0NLJBqiXzgHINviCNpijH+Sz4QEk5eXqCEmUcLD9+JoBuDirLfxh+fCgYsk4R+keqDma7fiItfpBpgC5Lslz77PhfTeaWYoOQHqVcC7dIyuiDSEWHQoYR+AG5GXFYrEDeUEVMm581OXNNWJhTHX3AqAStIkw3sRxLrlbiU82GlyuVxX2ZhEdJdxsbhznOW5TrH592BIyU6FAfEgbJ6UHFx7R/KFcvIt8Gjbl9OOcDSTcN1HlWE5L9KNzwX8Rp0Yj/eRRAgwF71/PvyFwC3RtSJW1i+iDxXgbn7VXZ6FJSeEm+hRYH18eseKYLPqALoUqbq7zt6Cdx3QisWyg6CD9zr/AMYW1G91y8l0p5RaFeX89WCQ6NG8pykSp7SrcJPv2C5Q6Em3cTZWIcDd+FHB4BcAnVh5qetMARZrHEh6+wJ3uEdWR7S6lJGg9XDxHq02Jhq89mPNDeCRW6LnU7D0yZyCXVoCNLEDrFuvG0aTk4J1tZEmhdvvFTJJ8ESG4W2zGkJF471BRgtiJhHQSw8Wtc1TG7Q/SfQkSAaAlpTCUXTdgUAAAgNPULiyMI5KmQS/MD2V0keN65iqQdR4jI8q3knHJWSCG+ZvBtRotwXiGaKGKQE8Ph7N4qwFJIFxMnB6JUzUrvH1Z6Se1WSZIPNN0xyOKMuqDnU9XxfzywGgcgpX+e2ouHAjQyQszDHsdYoDyErKgiV1aBEsQxrn6F6caWyiEOdE/f8Az2vYJKd9R4P8qILKBfBE8Jtyub8S0gcI6noDYEIkiVdw4gr8Iv0ycClomwA6A26ZrSRkpylKPD24vxRcLip+rSJZ0SOoT5oJzWHnloi+aJwS48nlV4SkN1usrxfWQiWtDEG2xq8mpviXhkm+97vvJjA7ATTns9KvhEFFabwZZZNMmzbdqQNx9VZgQiSJS5NvKd7wetOkzZR5akgOyB4qKSP/AOBkPFFSzTdo9iO2lNpY2Dhl4F616P0XY/A6FGQhBofAqURp6H9UsGZO65o8TrNWI1cn4hy6h8ILckMOrSAg4I9THN7a1dj5Co63vfdoxBwCx8W7eGE3HSmqEQl0G/TtRnEiaBwz7zRqVy9vudxQhPOldxmgAUI4T0fABMi+WoUvg2fU/VBDFw3YaDu0TT1Z+jYOQpKGdxN7gfr4+aLS0vJyclPq10bXe/mp5C5n9J5pa24/sFIpA5KloJcs/wBDV8bOv6H+UqDLXn2x4oAILH/f/9k=';

  constructor(private camera: Camera, private deviceService: DeviceService) {
    console.log('Hello PhotoService Provider');
  }

  pictureFromCamera() : Promise<any> {
      const options: CameraOptions = {
          quality: 100,
          destinationType: this.camera.DestinationType.DATA_URL,
          allowEdit: false,
          encodingType: this.camera.EncodingType.JPEG,
          mediaType: this.camera.MediaType.PICTURE,
          correctOrientation: true,
          saveToPhotoAlbum: true
      }

      //Take photo
      return this.takePhoto(options).then((imageData) => {
        //refactor this callback later
        let photo = this.initPhotoEntity();
        photo.rawData = imageData;
        photo.description = 'pictureFromCamera';
        //get user information later
        photo.maker = 'Root';
        return Promise.resolve(photo);
      }, (err: Error) => {
        return Promise.reject(`Can not take picture ${err}`);
      });
  }

  pictureFromGallery() : Promise<any> {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      allowEdit: false,
      encodingType: this.camera.EncodingType.JPEG,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      saveToPhotoAlbum: true
    }

    //Take photo 
    return this.takePhoto(options).then((imageData) => {
        //refactor this callback later
        let photo = this.initPhotoEntity();
        photo.rawData = imageData;
        photo.description = 'pictureFromGallery';
        //get user information later
        photo.maker = 'Root';
        return Promise.resolve(photo);
    }, (err: Error) => {
        return Promise.reject(`Can not take picture ${err}`);
    });

  }

  async takePhoto(options: CameraOptions) {
    try {
      //take photo and store result
      // this.getPhotoDevice(successCb, options) : ;
      const result  = this.deviceService.isDevice() ? await this.camera.getPicture(options) : await this.sendFakePhoto();
      return `data:image/jpeg;base64,${result}`;;
    }
    catch (e) {
      console.log(e);
      return e;
    }
  }

  sendFakePhoto() : string {
    console.log('sending fake photo');
    return this.FAKE_PHOTO_JPEG;
  }

  initPhotoEntity(): Photo {
    const photoEntity : Photo = {
      rawData: '',
      description: '',
      randomID: Math.random(),
      //To do: add location provider later
      lat: 0,
      lon: 0,
      maker: ''
    };
    return photoEntity;
  }

  // To be fix: ?????????cant return from this function???
  //generate thumbnail and put into photo entity here
  generateThumbnail(photo: Photo, callback: any) : void {

    let img: any = document.createElement('img');
    // when the event "onload" is triggered, we resize the image.
    img.onload = () => {
      // create a canvas and get its context
      let canvas: any = document.createElement('canvas');
      let ctx: any = canvas.getContext('2d');
      // set the dimensions
      canvas.width = this.thumbnailTargetWidth;
      canvas.height = this.thumbnailTargetHeight;
      // resize the image with the canvas
      ctx.drawImage(this, 0, 0, this.thumbnailTargetWidth, this.thumbnailTargetHeight);
      const resizedDataUri: string = canvas.toDataURL();
      photo.thumbnailData = resizedDataUri;
      callback(photo);
    };
    // put the data uri in the image's src attribute, which triggers the onload
    // img.src = 'data:image/jpeg;base64,' + photo.rawData;
    img.src = photo.rawData;
  }

  getPhotoDeviceSuccessGenerateThumbnail(photo: Photo): Photo {
    return photo;
  }



  // readEXIFData(imageData, photo) {
  //   var image = new Image();
  //   image.onload = function() {
  //     EXIF.getData(image, function() {
  //       var lat = EXIF.getTag(ps, "GPSLatitude");
  //       var make = EXIF.getTag(ps, "DateTimeOriginal");
  //       var lon = EXIF.getTag(ps, "GPSLongitude");
  //       //Convert coordinates to WGS84 decimal
  //       var latRef = EXIF.getTag(ps, "GPSLatitudeRef");
  //       var lonRef = EXIF.getTag(ps, "GPSLongitudeRef");
  //       lat = (lat[0] + lat[1] / 60 + lat[2] / 3600) * (latRef == "N" ? 1 : -1);
  //       lon = (lon[0] + lon[1] / 60 + lon[2] / 3600) * (lonRef == "W" ? -1 : 1);
  //       photo.lat = lat;
  //       photo.lon = lon;
  //     });
  //   };
  //   image.src = "data:image/jpeg;base64," + imageData;
  // };













}
