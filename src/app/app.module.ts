import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { NgModule, ErrorHandler } from '@angular/core';

import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';

import { InAppBrowser } from '@ionic-native/in-app-browser';
import { SplashScreen } from '@ionic-native/splash-screen';

import { IonicStorageModule } from '@ionic/storage';

import { ConferenceApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { PopoverPage } from '../pages/about-popover/about-popover';
import { AccountPage } from '../pages/account/account';
import { LoginPage } from '../pages/login/login';
import { MapPage } from '../pages/map/map';
import { SchedulePage } from '../pages/schedule/schedule';
import { ScheduleFilterPage } from '../pages/schedule-filter/schedule-filter';
import { SessionDetailPage } from '../pages/session-detail/session-detail';
import { SignupPage } from '../pages/signup/signup';
import { SpeakerDetailPage } from '../pages/speaker-detail/speaker-detail';
import { SpeakerListPage } from '../pages/speaker-list/speaker-list';
import { TabsPage } from '../pages/tabs-page/tabs-page';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { SupportPage } from '../pages/support/support';
//refactor to lazy load later
import { VisitedFamiliesPage } from '../pages/families/visited-families/visited-families';
import { FamilyPage } from '../pages/families/family/family';
import { FamiliesPage } from '../pages/families/families';
import { SettingsPage } from '../pages/settings/settings';
import { SurburbsPage } from '../pages/surburbs/surburbs';
import { FamiliesTabsPage } from '../pages/families-tabs/families-tabs.page';

import {
  DeviceService,
  PhotoService,
  CallService
} from '../providers/providers';


import { ConferenceData } from '../providers/conference-data';
import { UserData } from '../providers/user-data';

import { Calendar } from '@ionic-native/calendar';
import { Device } from '@ionic-native/device';
import { Camera } from '@ionic-native/camera';
import { CallNumber } from '@ionic-native/call-number';

@NgModule({
  declarations: [
    ConferenceApp,
    AboutPage,
    AccountPage,
    LoginPage,
    MapPage,
    PopoverPage,
    SchedulePage,
    ScheduleFilterPage,
    SessionDetailPage,
    SignupPage,
    SpeakerDetailPage,
    SpeakerListPage,
    TabsPage,
    TutorialPage,
    SupportPage,
    VisitedFamiliesPage,
    FamilyPage,
    FamiliesPage,
    SettingsPage,
    SurburbsPage,
    FamiliesTabsPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(ConferenceApp, {}, {
      links: [
        { component: TabsPage, name: 'TabsPage', segment: 'tabs-page' },
        { component: SchedulePage, name: 'Schedule', segment: 'schedule' },
        { component: SessionDetailPage, name: 'SessionDetail', segment: 'sessionDetail/:sessionId' },
        { component: ScheduleFilterPage, name: 'ScheduleFilter', segment: 'scheduleFilter' },
        { component: SpeakerListPage, name: 'SpeakerList', segment: 'speakerList' },
        { component: SpeakerDetailPage, name: 'SpeakerDetail', segment: 'speakerDetail/:speakerId' },
        { component: MapPage, name: 'Map', segment: 'map' },
        { component: AboutPage, name: 'About', segment: 'about' },
        { component: TutorialPage, name: 'Tutorial', segment: 'tutorial' },
        { component: SupportPage, name: 'SupportPage', segment: 'support' },
        { component: LoginPage, name: 'LoginPage', segment: 'login' },
        { component: AccountPage, name: 'AccountPage', segment: 'account' },
        { component: SignupPage, name: 'SignupPage', segment: 'signup' },
        { component: VisitedFamiliesPage, name: 'VisitedFamiliesPage', segment: 'visitedFamiliesPage' },
        { component: FamilyPage, name: 'FamilyPage', segment: 'familyPage' },
        { component: FamiliesPage, name: 'FamiliesPage', segment: 'familiesPage' },
        { component: SettingsPage, name: 'SettingsPage', segment: 'settingsPage' },
        { component: SurburbsPage, name: 'SurburbsPage', segment: 'surburbsPage' },
        { component: FamiliesTabsPage, name: 'FamiliesTabsPage', segment: 'familiesTabsPage' }
      ]
    }),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    ConferenceApp,
    AboutPage,
    AccountPage,
    LoginPage,
    MapPage,
    PopoverPage,
    SchedulePage,
    ScheduleFilterPage,
    SessionDetailPage,
    SignupPage,
    SpeakerDetailPage,
    SpeakerListPage,
    TabsPage,
    TutorialPage,
    SupportPage,
    VisitedFamiliesPage,
    FamilyPage,
    FamiliesPage,
    SettingsPage,
    SurburbsPage,
    FamiliesTabsPage
  ],
  providers: [
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ConferenceData,
    UserData,
    InAppBrowser,
    SplashScreen,
    Calendar,
    DeviceService,
    PhotoService,
    CallService,
    Device,
    Camera,
    CallNumber
  ]
})
export class AppModule { }
