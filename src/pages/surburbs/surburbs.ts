import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {User} from '../../assets/data/user.interface';
import families from '../../assets/data/families_mock';
import { PhotoService } from '../../providers/photo/photo.service';
import {Photo} from '../../assets/data/photo.interface';
import { CallService } from '../../providers/call/call.service';

/**
 * Generated class for the SurburbsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-surburbs',
  templateUrl: 'surburbs.html',
})
export class SurburbsPage implements OnInit {

  familiesCollection: {familyID: string, users: User[], surburb: number, address: string}[];
  photo: Photo;
  image: string;

  ngOnInit() {
    this.familiesCollection = families;
    console.log('test here', this.familiesCollection);
  }

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private photoService: PhotoService,
    private callService: CallService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SurburbsPage');
  }

  async takePhotofromcamera() {
     console.log('takePhotofromcamera');
     this.photo = await this.photoService.pictureFromCamera();
     this.image = this.photo.rawData;
     console.log(this.photo);
  }

  async takePhotofromgallery() {
    console.log('takePhotofromgallery');
    this.photo  = await this.photoService.pictureFromGallery();
    this.image = this.photo.rawData;
    console.log(this.photo);
  }

  //To be fixed..
  //call this for test, only call if this.photo exists already
  takeThumbnail() {
    // this.photoService.generateThumbnail(this.photo, this.photoService.getPhotoDeviceSuccessGenerateThumbnail(this.photo));
  }

  //To do: get real number from contact and call it later
  async  call() {
    const number: string = '0425306362';
   try  {
      await this.callService.makeCall(number);
   }
   catch (e) {
     console.log(e);
   }
    

   
    

  }

}
